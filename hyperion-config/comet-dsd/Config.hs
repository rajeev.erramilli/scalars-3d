-- To use, create a symlink 'src/Config.hs' pointing to this file

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Config where

import           Data.FileEmbed (makeRelativeToProject, strToExp)
import           Hyperion
import qualified Hyperion.Slurm as Slurm

scriptsDir :: FilePath
scriptsDir = $(makeRelativeToProject "comet-dsd" >>= strToExp)

config :: HyperionConfig
config = (defaultHyperionConfig "/oasis/scratch/comet/dsd/temp_project/hyperion")
  { emailAddr = Just "dsd@caltech.edu"
  , defaultSbatchOptions = Slurm.defaultSbatchOptions
    { Slurm.partition = Just "compute" }
  }
