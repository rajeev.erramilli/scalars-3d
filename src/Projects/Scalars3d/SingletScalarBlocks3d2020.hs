{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE OverloadedRecordDot   #-}

module Projects.Scalars3d.SingletScalarBlocks3d2020 where

import qualified Blocks.ScalarBlocks                    as SB
import           Bootstrap.Bounds.Spectrum              (setGap,
                                                         unitarySpectrum)
import           Bounds.Scalars3d.SingletScalarBlocks3d (SingletScalar (..))
import qualified Bounds.Scalars3d.SingletScalarBlocks3d as SS
import           Control.Monad.IO.Class                 (liftIO)
import           Control.Monad.Reader                   (local)
import           Data.Aeson                             (ToJSON)
import           Data.Binary                            (Binary)
import           Data.Proxy                             (Proxy (..))
import           Data.Scientific                        (Scientific)
import           Data.Text                              (Text)
import           Data.Vector                            (Vector)
import           GHC.Generics                           (Generic)
import           Hyperion
import           Hyperion.Bootstrap.BinarySearch        (BinarySearchConfig (..),
                                                         Bracket (..))
import           Hyperion.Bootstrap.Bound               (BinarySearchBound (..),
                                                         Bound (..),
                                                         BoundFiles (..))
import qualified Hyperion.Bootstrap.Bound               as Bound
import           Hyperion.Bootstrap.Main                (unknownProgram)
import qualified Hyperion.Bootstrap.Params              as Params
import qualified Hyperion.Database                      as DB
import qualified Hyperion.Log                           as Log
import           Hyperion.Util                          (minute)
import           Numeric.Rounded                        (Rounded,
                                                         RoundingMode (..))
import           Projects.Scalars3d.Defaults            (defaultBoundConfig)
import           SDPB                                   (Params (..))
import qualified SDPB

data SingletScalarBinarySearch = SingletScalarBinarySearch
  { ssbs_bound     :: Bound Int SingletScalar
  , ssbs_config    :: BinarySearchConfig Rational
  , ssbs_gapSector :: Int
  } deriving (Show, Generic, Binary, ToJSON)

remoteSingletScalarBinarySearch :: SingletScalarBinarySearch -> Cluster (Bracket Rational)
remoteSingletScalarBinarySearch ssbs = Bound.remoteBinarySearchBound MkBinarySearchBound
  { bsBoundClosure      = static mkBound `ptrAp` cPure (ssbs_bound ssbs) `cAp` cPure (ssbs_gapSector ssbs)
  , bsConfig            = ssbs_config ssbs
  , bsResultBoolClosure = cPtr (static getBool)
  }
  where
    mkBound bound gapSector gap =
      bound { boundKey = (boundKey bound) { spectrum = gappedSpectrum } }
      where
        gappedSpectrum = setGap gapSector gap (spectrum (boundKey bound))
    getBool bound files result = do
      case SDPB.isDualFeasible result of
        False -> pure ()
        True  -> saveVectors bound files result
      pure $ not (SDPB.isDualFeasible result)

-- Differs from SingletScalar in that it saves the coefficients in derivative basis
saveVectors :: Bound Int SingletScalar -> BoundFiles -> SDPB.Output -> Job ()
saveVectors bound' boundFiles result =
  Bound.reifyPrecisionWithFetchContext bound' (Proxy @Job) bound'.precision
  $ \(_ :: Proxy p) -> do
  let cftB = (bound' :: Bound Int SingletScalar) { Bound.precision = Proxy @p }
  norm <- Bound.getBoundObject @Job @SingletScalar @p @(Vector (Rounded 'TowardZero p)) cftB boundFiles $
    SDPB.normalizationVector . SDPB.normalization . Bound.toSDP
  alphaVec <- liftIO $ SDPB.readFunctional norm (outDir boundFiles)
  Log.text "Saving functional"
  DB.insert functionalVectors bound' (alphaVec, SDPB.dualObjective result)
  where
    functionalVectors :: DB.KeyValMap (Bound Int SingletScalar) (Vector a, Scientific)
    functionalVectors = DB.KeyValMap "functional_vectors"

singletScalarDefaultGaps :: Int -> Rational -> SingletScalar
singletScalarDefaultGaps nmax dPhi = SingletScalar
  { spectrum     = unitarySpectrum
  , objective    = SS.Feasibility
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.spinsNmax nmax
  , deltaPhi     = dPhi
  }

boundsProgram :: Text -> Cluster ()

boundsProgram "SingletScalarB3dAllowed_test_nmax6" =
  local (setJobType (MPIJob 1 6) . setJobTime (25*minute)) $
  mapConcurrently_ (Bound.remoteCompute . go)
  [ (0.5181489, 1.4) -- Should be allowed
  , (0.5181489, 1.5) -- Should be disallowed
  ]
  where
    nmax = 6
    go (dPhi, deltaS) = Bound
      { boundKey = (singletScalarDefaultGaps nmax dPhi)
        { spectrum = setGap 0 deltaS unitarySpectrum
        }
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "SingletScalarB3d_binary_search_test_nmax6" =
  mapConcurrently_ search
  [ (0.5181489, 6, MPIJob 1 6, 30*minute, 768) ]
  where
    search (dPhi, nmax, jobType, jobTime, prec) =
      local (setJobType jobType . setJobTime jobTime) $
      remoteSingletScalarBinarySearch $
      SingletScalarBinarySearch
      { ssbs_bound = Bound
        { boundKey = singletScalarDefaultGaps nmax dPhi
        , precision = (Params.blockParamsNmax nmax).precision
        , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = prec }
        , boundConfig = defaultBoundConfig
        }
      , ssbs_gapSector = 0
      , ssbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 1
          , falsePoint = 8
          }
        , threshold  = 1e-30
        , terminateTime = Nothing
        }
      }

boundsProgram p = unknownProgram p
