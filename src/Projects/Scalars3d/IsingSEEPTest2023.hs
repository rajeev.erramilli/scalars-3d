{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE OverloadedRecordDot   #-}

module Projects.Scalars3d.IsingSEEPTest2023 where

import qualified Blocks.ScalarBlocks                     as SB
import           Bounds.Scalars3d.IsingSigEpsEpsP                          (ExternalDims (..),
                                                              IsingSigEpsEpsP (..))
import qualified Bounds.Scalars3d.IsingSigEpsEpsP                          as Ising
import           Control.Concurrent                          (threadDelay)
import           Control.Monad                               (forM_, void, when)
import           Control.Monad.Catch                         (throwM)
import           Control.Monad.IO.Class                      (liftIO)
import           Control.Monad.Reader                        (asks, local)
import           Data.Aeson                                  (FromJSON, ToJSON)
import           Data.List.Split                             (chunksOf)
import qualified Data.Map.Strict                             as Map
import           Data.Proxy                                  (Proxy)
import           Data.Ratio                                  (approxRational)
import           Data.Reflection                             (reifyNat)
import           Data.Scientific                             (Scientific)
import qualified Data.Set                                    as Set
import           Data.Text                                   (Text)
import           Data.Typeable                               (Typeable)
import           Hyperion
import           Bootstrap.Math.AffineTransform      (AffineTransform (..))
import qualified Bootstrap.Bounds.Spectrum               as Spectrum
import           Bootstrap.Math.Linear                   (diagonalMatrix, fromV,
                                                          toV)
import           Hyperion.Bootstrap.Bound                (Bound (..),
                                                          BoundFiles (..),CheckpointMap, LambdaMap)
import qualified Hyperion.Bootstrap.Bound                as Bound
import           Hyperion.Bootstrap.DelaunaySearch       (delaunaySearchRegionPersistent)
import           Hyperion.Bootstrap.Main                 (unknownProgram)
import           Hyperion.Bootstrap.OPESearch            (BilinearForms (..),
                                                          OPESearchConfig (..),
                                                          TrackedMap)
import qualified Hyperion.Bootstrap.OPESearch            as OPE
import qualified Hyperion.Bootstrap.Params               as Params
import qualified Hyperion.Bootstrap.SDPDeriv             as SDPDeriv
import qualified Hyperion.Bootstrap.SDPDeriv.NewtonTrust as NewtonTrust
import qualified Hyperion.Database                       as DB
import qualified Hyperion.Log                            as Log
import qualified Hyperion.Slurm                          as Slurm
import           Hyperion.Util                           (hour, minute)
import           Linear.V                                (V)
import           Numeric.Rounded                         (Rounded,
                                                          RoundingMode (..))
import           Projects.Scalars3d.Defaults             (defaultBoundConfig,
                                                          defaultDelaunayConfig)
import           SDPB                                    (Params (..))
import qualified SDPB
import           Blocks.ScalarBlocks        (ScalarBlockParams (..))
--import           Hyperion
--import           Hyperion.Bootstrap.AffineTransform          (AffineTransform (..))
--import           Hyperion.Bootstrap.CFTBound                 (CFTBound (..),
--                                                              CFTBoundFiles (..),
--                                                              checkpointDir,
--                                                              computeCFTBoundClean',
--                                                              computeCFTBoundWithFileTreatment,
--                                                              remoteComputeBound,
--                                                              remoteComputeBoundWithFileTreatment,
--                                                              defaultCFTBoundFiles,
--                                                              keepAllFiles,
--                                                              keepOutAndCheckpoint,
--                                                              outDir,
--                                                              setFixedTimeLimit, CheckpointMap, 
--                                                              LambdaMap)
--import           Hyperion.Bootstrap.CFTBound.ParamCollection (blockParamsNmax,
--                                                              jumpFindingParams,
--                                                              optimizationParams,
--                                                              sdpbParamsNmax,
--                                                              spinsNmax)
--import           Hyperion.Bootstrap.DelaunaySearch           (delaunaySearchRegionPersistent)
--import           Hyperion.Bootstrap.Main                     (UnknownProgram (..))
--import           Hyperion.Bootstrap.OPESearch                (BilinearForms (..),
--                                                              OPESearchConfig (..),
--                                                              TrackedMap)
--import qualified Hyperion.Bootstrap.OPESearch                as OPE
--import qualified Hyperion.Bootstrap.SDPDeriv                 as SDPDeriv
--import qualified Hyperion.Bootstrap.SDPDeriv.NewtonTrust     as NewtonTrust
--import qualified Hyperion.Bootstrap.SDPDeriv.BFGS            as BFGS
--import qualified Hyperion.Database                           as DB
--import qualified Hyperion.Log                                as Log
--import qualified Hyperion.Slurm                              as Slurm
--import           Hyperion.Util                               (hour, minute)
--import           Linear.V                                    (V)
--import           Numeric.Rounded                             (Rounded,
--                                                              RoundingMode (..))
--import           Projects.Defaults                           (defaultBoundConfig,
--                                                              defaultDelaunayConfig,
--                                                              defaultQuadraticNetConfig)
--import qualified SDPB.Blocks.ScalarBlocks                    as SB
--import           SDPB.Bounds.BoundDirection                  (BoundDirection (..))
--import           SDPB.Bounds.Spectrum                        (setGaps,
--                                                              setTwistGap,
--                                                              unitarySpectrum)
--import           SDPB.Math.Linear                            (diagonalMatrix,
--                                                              fromV, toV)
--import           SDPB.Solver                                 (Params (..))
--import qualified SDPB.Solver                                 as SDPB
import           System.Directory                            (createDirectoryIfMissing,
                                                              removePathForcibly)
import           Hyperion.Bootstrap.Main                 (unknownProgram)
import           Projects.Scalars3d.Defaults             (defaultBoundConfig,defaultQuadraticNetConfig,
                                                          defaultDelaunayConfig)

sdpdIsingSEEP
  :: Bound Int IsingSigEpsEpsP
  -> BoundFiles
  -> Bound Int IsingSigEpsEpsP
  -> BoundFiles
  -> Job (SDPDeriv.Output Scientific)
sdpdIsingSEEP bound1 files1 bound2 files2 = do
  let prec = SDPB.precision (solverParams bound1)
  reifyNat (fromIntegral prec) $ \(_ :: Proxy p) -> do
    out :: SDPDeriv.Output (Rounded 'TowardZero p) <- SDPDeriv.sdpDerivBoundPair bound1 files1 bound2 files2
    pure (fmap realToFrac out)



remoteIsingSEEPOPESearch
  :: CheckpointMap IsingSigEpsEpsP
  -> LambdaMap IsingSigEpsEpsP (V 6 Rational)
  -> BilinearForms 6
  -> Bound Int IsingSigEpsEpsP 
  -> Cluster Bool
remoteIsingSEEPOPESearch checkpointMap lambdaMap initialBilinearForms =
  OPE.remoteOPESearch (cPtr (static opeSearchCfg)) checkpointMap lambdaMap initialBilinearForms
  where
    opeSearchCfg =
      OPESearchConfig setLambda Ising.getExternalMat (OPE.queryAllowedMixed qmConfig)
    setLambda :: (V 6 Rational) -> Bound prec IsingSigEpsEpsP-> Bound prec IsingSigEpsEpsP 
    setLambda lambda cftBound = cftBound
      { boundKey = (boundKey cftBound)
        { Ising.objective = Ising.Feasibility (Just lambda) }
      }
    qmConfig = OPE.QueryMixedConfig
      { qmQuadraticNetConfig = defaultQuadraticNetConfig
      , qmDescentConfig      = OPE.defaultDescentConfig
      , qmResolution         = 1e-16
      , qmPrecision          = 384
      , qmHessianLineSteps   = 200
      }

remoteComputeIsingSEEPBounds :: [Bound Int IsingSigEpsEpsP] -> Cluster ()
remoteComputeIsingSEEPBounds cftBounds = do
  -- Wait between 15 seconds and an hour. Hopefully this relieves
  -- pressure on the database when submitting a large number of
  -- jobs...
  -- t <- liftIO $ randomRIO (15, 3600)
  -- liftIO $ threadDelay $ t*1000*1000
  workDir <- newWorkDir (head cftBounds)
  jobTime <- asks (Slurm.time . clusterJobOptions)
  remoteEvalJob $
    static compute `ptrAp` cPure jobTime `cAp` cPure cftBounds `cAp` cPure workDir
  where
    computationsMap :: DB.KeyValMap (Bound Int IsingSigEpsEpsP) SDPB.Output
    computationsMap = DB.KeyValMap "computations"
    compute jobTime cftBounds' workDir' = do
      -- We ask SDPB to terminate within 90% of the jobTime. This
      -- should ensure sufficiently prompt exit as long as an
      -- iteration doesn't take 10% or more of the jobTime.
      solverParams' <- liftIO $ Bound.setFixedTimeLimit (0.9*jobTime) (solverParams (head cftBounds'))
      forM_ (zip cftBounds' [1 :: Int ..]) $ \(cftBound,i) ->
        DB.lookup computationsMap cftBound >>= \case
          Just _  -> pure ()
          Nothing -> do
            Log.text $ "Computing bound " <> Log.showText i <> " of " <> Log.showText (length cftBounds') <> "."
            let boundFiles = Bound.defaultBoundFiles workDir'
            result <- Bound.computeClean' (cftBound { solverParams = solverParams' }) boundFiles
            when (SDPB.isFinished result) $
              DB.insert computationsMap cftBound result
            liftIO $ mapM_ removePathForcibly [Bound.checkpointDir boundFiles, Bound.outDir boundFiles]

isingDimVector :: Bound prec IsingSigEpsEpsP -> V 3 Rational
isingDimVector Bound{ boundKey = i } =
  toV (deltaSigma (externalDims i), deltaEps (externalDims i), deltaEpsP (externalDims i))

newIsingCheckpointMap
 :: AffineTransform 3 Rational
 -> Maybe FilePath
 -> Cluster (TrackedMap Cluster (Bound Int IsingSigEpsEpsP) FilePath)
newIsingCheckpointMap affine mCheckpoint =
 liftIO (OPE.affineLocalityMap affine isingDimVector mCheckpoint) >>=
 OPE.mkPersistent (DB.KeyValMap "isingCheckpoints")

newIsingLambdaMap
  :: (FromJSON l, ToJSON l, Typeable l)
  => AffineTransform 3 Rational
  -> Maybe l
  -> Cluster (TrackedMap Cluster (Bound Int IsingSigEpsEpsP) l)
newIsingLambdaMap affine mLambda =
 liftIO (OPE.affineLocalityMap affine isingDimVector mLambda) >>=
 OPE.mkPersistent (DB.KeyValMap "isingLambdas")


delaunaySearchPoints :: DB.KeyValMap (V n Rational) (Maybe Bool)
delaunaySearchPoints = DB.KeyValMap "delaunaySearchPoints"

isingFeasibleDefaultGaps :: V 3 Rational -> V 6 Rational -> Int -> IsingSigEpsEpsP
isingFeasibleDefaultGaps deltaExts lambda nmax = IsingSigEpsEpsP
  { spectrum     = Spectrum.setGaps [ ((0, Ising.Z2Even), 5)
                           , ((0, Ising.Z2Odd),  4) 
                           , ((2, Ising.Z2Even), 5)] . Spectrum.setTwistGap 1e-10 $ Spectrum.unitarySpectrum
  , objective    = Ising.Feasibility (Just lambda)
  , externalDims = Ising.ExternalDims {..}
  , blockParams  = Params.blockParamsNmax nmax
  , spins        = Params.spinsNmax nmax
  }
  where
    (deltaSigma, deltaEps, deltaEpsP) = fromV deltaExts

--isingSEEPGFFNavigatorDefaultGaps :: V 3 Rational -> Maybe (V 4 Rational) -> Int -> IsingSigEpsEpsP
--isingSEEPGFFNavigatorDefaultGaps deltaExts lambda nmax = IsingSigEpsEpsP
--  { spectrum     = Spectrum.setGaps [ ((0, Ising.Z2Even), 6)
--                           , ((0, Ising.Z2Odd),  5)
--                           --, ((1, Ising.Z2Odd), 5)
--                           , ((2, Ising.Z2Even),  5)] $ Spectrum.unitarySpectrum
--  , objective = Ising.GFFNavigator lambda
--  , externalDims = Ising.ExternalDims {..}
--  , blockParams  = Params.blockParamsNmax nmax
--  , spins        = Params.spinsNmax nmax
--  }
--  where
--    (deltaSigma, deltaEps, deltaEpsP) = fromV deltaExts
--
--boundsProgram :: Text -> Cluster ()
--
boundsProgram "IsingSigEpsEpsPAllowed_test_nmax6" =
  local (setJobType (MPIJob 1 8) . setJobTime (2*hour)) $ 
  mapConcurrently_ (Bound.remoteCompute.  bound)
  [ (dVic , mLambda )
  ,  (toV (0.517, 1.5, 3.8), mLambda)
  ]
  where
    nmax = 6
    dVic :: (V 3 Rational)
    dVic = toV (0.6, 2, 3)
    mLambda :: (V 6 Rational)
    mLambda = toV (-0.254569, -0.11829, -0.501378, -0.818389, 0.123, -0.231)
    specialSpinSet = [0..21] 
    bound (deltaExts, lambda) = Bound
      { boundKey = (isingFeasibleDefaultGaps deltaExts (lambda) nmax) --{spins = specialSpinSet, blockParams = ScalarBlockParams{keptPoleOrder = 8, order = 30, nmax =6, precision     = 768}}
      , precision = (Params.blockParamsNmax nmax).precision
      --, solverParams = (Params.sdpbParamsNmax nmax) { precision = 768 }
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 , primalErrorThreshold = 1e-80, dualErrorThreshold           = 1e-80, infeasibleCenteringParameter = 0.3}
      , boundConfig = defaultBoundConfig
      }
--
--boundsProgram "IsingSigEpsEpsPAllowed_test_nmax8" =
--  local (setJobType (MPIJob 1 16) . setJobTime (5*hour)) $
--  mapConcurrently_ (Bound.remoteCompute.  bound)
--  [ (dVic , mLambda )
--  --, (toV (0.518149, 1.412625), thetaVectorApprox 1e-16 0.96926)
--  ,  (toV (0.517, 1.5, 3.8), mLambda)
--  ]
--  where
--    nmax = 8
--    dVic :: (V 3 Rational)
--    dVic = toV (0.51803, 1.412625, 3.8)
--    mLambda :: (V 6 Rational)
--    mLambda = toV (-0.254569, -0.11829, -0.501378, -0.818389, 0.123, -0.231)
--    bound (deltaExts, lambda) = Bound
--      { boundKey = isingFeasibleDefaultGaps deltaExts (lambda) nmax
--      , precision = (Params.blockParamsNmax nmax).precision
--      , solverParams = (Params.sdpbParamsNmax nmax) { precision = 768 }
--      , boundConfig = defaultBoundConfig
--      }
--

--boundsProgram "IsingSigEpsEpsP_GFFNavigator_test_theta_nmax6" =
--  local (setJobType (MPIJob 1 16) . setJobTime (8*hour)) $
--  mapConcurrently_ (remoteComputeIsingSEEPBounds. map bound) $ chunksOf 50 $ do
--  deltaEps <- [1.412625]
--  deltaEpsP <- [3.8]
--  deltaSig <- [0.51803]
--  pure (deltaSig, deltaEps, deltaEpsP)
--  where
--    nmax = 6
--    mLambda :: Maybe (V 6 Rational)
--    mLambda = Just $ toV (-0.254569, -0.11829, -0.501378, -0.818389, 0.123, -0.231)
--    bound (deltaSig, deltaEps, deltaEpsP) = Bound
--      { boundKey = (isingSEEPGFFNavigatorDefaultGaps (toV (deltaSig, deltaEps, deltaEpsP)) mLambda nmax)
--      , precision = (Params.blockParamsNmax nmax).precision
--      , solverParams = (Params.optimizationParams nmax) { precision = 768 }
--      , boundConfig = defaultBoundConfig
--      }
--    specialSpinSet = [1..16] 
--
--boundsProgram "IsingSigEpsEpsP_GFFNavigator_test_theta_nmax8" =
--  local (setJobType (MPIJob 1 16) . setJobTime (8*hour)) $
--  mapConcurrently_ (remoteComputeIsingSEEPBounds. map bound) $ chunksOf 50 $ do
--  deltaEps <- [1.412625]
--  deltaEpsP <- [3]
--  deltaSig <- [0.51803]
--  pure (deltaSig, deltaEps, deltaEpsP)
--  where
--    nmax = 8
--    mLambda :: Maybe (V 6 Rational)
--    mLambda = Just $ toV (-0.254569, -0.11829, -0.501378, -0.818389, 0.123, -0.231)
--    bound (deltaSig, deltaEps, deltaEpsP) = Bound
--      { boundKey = isingSEEPGFFNavigatorDefaultGaps (toV (deltaSig, deltaEps, deltaEpsP)) mLambda nmax
--      , precision = (Params.blockParamsNmax nmax).precision
--      , solverParams = (Params.optimizationParams nmax) { precision = 768 }
--      , boundConfig = defaultBoundConfig
--      }
--

--
--boundsProgram "IsingSigEps_GFFNavigator_theta_eps_high_res_nmax6" =
--  local (setJobType (MPIJob 1 8) . setJobTime (8*hour)) $
--  mapConcurrently_ (remoteComputeIsingSigEpsBounds . map bound) $ chunksOf 50 $ do
--  let
--    deltaSig = 0.5181489
--  deltaEps <- [1.37, 1.371 .. 1.47]
--  thetaSigEps <- [0.859,0.861 .. 1.059]
--  pure (deltaSig, deltaEps, thetaSigEps)
--  where
--    nmax = 6
--    bound (deltaSig, deltaEps, th) = CFTBound
--      { boundKey = isingGFFNavigatorDefaultGaps (toV (deltaSig, deltaEps)) (Just (thetaVectorApprox 1e-16 th)) nmax
--      , precision = (blockParamsNmax nmax).precision
--      , solverParams = (optimizationParams nmax) { precision = 640 }
--      , boundConfig = defaultBoundConfig
--      }
--
--boundsProgram "IsingSigEps_GFFNavigator_sig_eps_no_lambda_nmax6" =
--  local (setJobType (MPIJob 1 8) . setJobTime (12*hour)) $
--  mapConcurrently_ (remoteComputeIsingSigEpsBounds . map bound) $ chunksOf 80 extraPts
--  where
--    pts1 = (,) <$> [0.515, 0.5151 .. 0.525] <*> [1.37, 1.371 .. 1.47]
--    pts2 = (,) <$> [0.51,  0.5101 .. 0.53]  <*> [1.3,  1.301 .. 1.5]
--    extraPts = Set.toList $ Set.fromList pts2 `Set.difference` Set.fromList pts1
--    nmax = 6
--    bound (deltaSig, deltaEps) = CFTBound
--      { boundKey = isingGFFNavigatorDefaultGaps (toV (deltaSig, deltaEps)) Nothing nmax
--      , precision = (blockParamsNmax nmax).precision
--      , solverParams = (optimizationParams nmax) { precision = 640 }
--      , boundConfig = defaultBoundConfig
--      }
--
--boundsProgram "IsingSigEps_GFFNavigator_sig_eps_no_lambda_nmax6_coarse" =
--  local (setJobType (MPIJob 1 8) . setJobTime (6*hour)) $
--  mapConcurrently_ (remoteComputeIsingSigEpsBounds . map bound) $ chunksOf 10 extraPts
--  where
--    pts1 = (,) <$> [0.515, 0.516 .. 0.525] <*> [1.37, 1.38 .. 1.47]
--    pts2 = (,) <$> [0.51,  0.511 .. 0.53]  <*> [1.3,  1.31 .. 1.5]
--    extraPts = Set.toList $ Set.fromList pts2 `Set.difference` Set.fromList pts1
--    nmax = 6
--    bound (deltaSig, deltaEps) = CFTBound
--      { boundKey = isingGFFNavigatorDefaultGaps (toV (deltaSig, deltaEps)) Nothing nmax
--      , precision = (blockParamsNmax nmax).precision
--      , solverParams = (optimizationParams nmax) { precision = 640 }
--      , boundConfig = defaultBoundConfig
--      }
--
--boundsProgram "IsingSigEps_GFFNavigator_test_nmax10" =
--  local (setJobType (MPIJob 1 24) . setJobTime (48*hour)) $
--  mapConcurrently_ (remoteComputeIsingSigEpsBounds . map bound) $ chunksOf 80 $ do
--  deltaSig <- [0.515, 0.5151 .. 0.525]
--  deltaEps <- [1.37, 1.371 .. 1.47]
--  -- deltaSig <- [0.515, 0.516 .. 0.525]
--  -- deltaEps <- [1.37, 1.38 .. 1.47]
--  pure (deltaSig, deltaEps, thetaSigEps)
--  where
--    nmax = 10
--    thetaSigEps = 0.96926
--    bound (deltaSig, deltaEps, th) = CFTBound
--      { boundKey = isingGFFNavigatorDefaultGaps (toV (deltaSig, deltaEps)) (Just (thetaVectorApprox 1e-16 th)) nmax
--      , precision = (blockParamsNmax nmax).precision
--      , solverParams = (optimizationParams nmax) { precision = 1024 }
--      , boundConfig = defaultBoundConfig
--      }

--boundsProgram "IsingSigEps_GFFNavigator_nmax6_sdp_derivative_test" =
--  local (setJobType (MPIJob 1 8) . setJobTime (1*hour)) $ do
--  results <- mapConcurrently myRemoteComputeBound bounds
--  remoteEvalJob $
--    static remoteDerivatives `ptrAp` cPure (zip bounds (map snd results))
--  where
--    bounds = do
--      deltaSig <- 0.518 : [0.518 + 0.01*(1/2) ^^ n | n <- [0::Int .. 25] ]
--      let deltaEps = 1.4
--      pure $ bound (deltaSig, deltaEps)
--    nmax = 6
--    bound (deltaSig, deltaEps) = CFTBound
--      { boundKey = isingGFFNavigatorDefaultGaps (toV (deltaSig, deltaEps)) Nothing nmax
--      , precision = (blockParamsNmax nmax).precision
--      , solverParams = (optimizationParams nmax)
--        { precision = 768
--        , dualityGapThreshold = 1e-30
--        , writeSolution = SDPB.allSolutionParts
--        }
--      , boundConfig = defaultBoundConfig
--      }
--
--    remoteDerivatives :: [(CFTBound Int IsingSigEps, CFTBoundFiles)] -> Job ()
--    remoteDerivatives boundsAndFiles = do
--      case boundsAndFiles of
--        [] -> error "No bounds"
--        (bound1, files1) : rest ->
--          forM_ rest $ \(bound2, files2) -> do
--          deriv <- sdpdIsingSigEps bound1 files1 bound2 files2
--          Log.info "Computed derivative" (bound1, bound2, deriv)
--          DB.insert (DB.KeyValMap "derivatives") (bound1, bound2) deriv
--
--    myRemoteComputeBound :: CFTBound Int IsingSigEps -> Cluster (SDPB.Output, CFTBoundFiles)
--    myRemoteComputeBound cftBound = do
--      workDir <- newWorkDir cftBound
--      liftIO $ createDirectoryIfMissing True workDir
--      let files = defaultCFTBoundFiles workDir
--      jobTime <- asks (Slurm.time . clusterJobOptions)
--      result <- remoteEvalJob $
--        static compute `ptrAp` cPure jobTime `cAp` cPure cftBound `cAp` cPure files
--      Log.info "Computed" (cftBound, result)
--      DB.insert (DB.KeyValMap "computations") cftBound result
--      pure (result, files)
--        where
--          compute jobTime cftBound' files' = do
--            solverParams' <- liftIO $ setFixedTimeLimit (0.9*jobTime) (solverParams cftBound')
--            _ <- computeCFTBoundWithFileTreatment
--              keepAllFiles
--              cftBound' { solverParams = solverParams' }
--              files'
--            computeCFTBoundWithFileTreatment
--              keepAllFiles
--              cftBound' { solverParams = SDPDeriv.setCentralPathParams 10 solverParams' }
--              files'
--
---- The navigator appears to have too many places where the hessian is
---- discontinuous for this naive Newton search (with trust region) to
---- work. So far BFGS is the most robust method.
--boundsProgram "IsingSigEps_GFFNavigator_Newton_nmax6_test" =
--  local (setJobType (MPIJob 1 8) . setJobTime (2*hour)) $ do
--  result :: (V 2 Rational, [SDPDeriv.Jet2 n (Rounded 'TowardZero 200)]) <-
--    SDPDeriv.remoteNewtonTrustBound nsConfig bjConfig
--  Log.info "Finished Newton search" result
--  where
--    trustNorm xWidth yWidth = diagonalMatrix $ toV (1/xWidth^(2::Int), 1/yWidth^(2::Int))
--    nsConfig = NewtonTrust.defaultConfig $ trustNorm 0.001 0.01
--    bjConfig = SDPDeriv.GetBoundJetConfig
--      { centeringIterations = 10
--      , fileTreatment = keepOutAndCheckpoint
--      , boundClosure = cPtr (static mkBound)
--      , valFromObjClosure = cPtr (static (SDPDeriv.MkFractionalMap (\y -> y/(1-y))))
--      , initialPoint = toV (0.512, 1.34)
--      , checkpointMapName = Nothing
--      }
--    mkBound :: V 2 Rational -> CFTBound Int IsingSigEps
--    mkBound v = CFTBound
--      { boundKey = isingGFFNavigatorDefaultGaps v Nothing nmax
--      , precision = (blockParamsNmax nmax).precision
--      , solverParams = (optimizationParams nmax)
--        { precision = 768
--        , dualityGapThreshold = 1e-30
--        }
--      , boundConfig = defaultBoundConfig
--      }
--      where
--        nmax = 6
--
--boundsProgram "IsingSigEps_GFFNavigator_BFGS_nmax6_test" =
--  local (setJobType (MPIJob 1 8) . setJobTime (4*hour) ) $ 
--  do
--    result :: (V 1 Rational, [BFGS.BFGSData n (Rounded 'TowardZero 200)]) <-
--      SDPDeriv.remoteBFGSBound bfgsConfig bjConfig
--    Log.info "Finished BFGS search" result
--  where
--    bbMin = toV (0.95)
--    bbMax = toV (0.98)
--    bfgsConfig = BFGS.MkConfig
--      { BFGS.epsGradHess         = 1e-9
--      , BFGS.stepResolution      = 1e-32
--      , BFGS.gradNormThreshold   = 1e-7
--      , BFGS.stopOnNegative      = False
--      , BFGS.boundingBoxMin      = bbMin
--      , BFGS.boundingBoxMax      = bbMax
--      , BFGS.initialHessianGuess = Nothing
--      }
--    bjConfig = SDPDeriv.GetBoundJetConfig
--      { centeringIterations = 10
--      , fileTreatment = keepOutAndCheckpoint
--      , boundClosure = cPtr (static mkBound)
--      , valFromObjClosure = cPtr (static (SDPDeriv.MkFractionalMap (\y -> y/(1-y))))
--      , initialPoint = toV (0.95926)
--      , checkpointMapName = Nothing
--      }
--    mkBound theta = CFTBound
--      { boundKey = isingGFFNavigatorDefaultGaps deltaExts (Just lambda) nmax
--      , precision = (blockParamsNmax nmax).precision
--      , solverParams = (optimizationParams nmax)
--        { precision = 768
--        , dualityGapThreshold = 1e-30
--        }
--      , boundConfig = defaultBoundConfig
--      }
--      where
--        nmax = 6
--        deltaExts = toV (0.518149, 1.412625)
--        th = fromV theta 
--        lambda = thetaVectorApprox 1e-16 th 
--
--boundsProgram "IsingSigEps_GFFNavigator_BFGS_nmax6_3d_test" =
--  local (setJobType (MPIJob 1 8) . setJobTime (24*hour) ) $ 
--  do
--    result :: (V 3 Rational, [BFGS.BFGSData n (Rounded 'TowardZero 200)]) <-
--      SDPDeriv.remoteBFGSBound bfgsConfig bjConfig
--    Log.info "Finished BFGS search" result
--  where
--    bbMin = toV (0.515, 1.2, 0.93) 
--    bbMax = toV (0.55, 1.8, 0.999)
--    bfgsConfig = BFGS.MkConfig
--      { BFGS.epsGradHess         = 1e-9
--      , BFGS.stepResolution      = 1e-32
--      , BFGS.gradNormThreshold   = 1e-7
--      , BFGS.stopOnNegative      = False
--      , BFGS.boundingBoxMin      = bbMin
--      , BFGS.boundingBoxMax      = bbMax
--      , BFGS.initialHessianGuess = Nothing
--      }
--    bjConfig = SDPDeriv.GetBoundJetConfig
--      { centeringIterations = 10
--      , fileTreatment = keepOutAndCheckpoint
--      , boundClosure = cPtr (static mkBound)
--      , valFromObjClosure = cPtr (static (SDPDeriv.MkFractionalMap (\y -> y/(1-y))))
--      , initialPoint = toV (0.519, 1.5, 0.966)
--      , checkpointMapName = Nothing
--      }
--    mkBound :: V 3 Rational -> CFTBound Int IsingSigEps
--    mkBound v = CFTBound
--      { boundKey = isingGFFNavigatorDefaultGaps deltaExts (Just lambda) nmax
--      , precision = (blockParamsNmax nmax).precision
--      , solverParams = (optimizationParams nmax)
--        { precision = 768
--        , dualityGapThreshold = 1e-30
--        }
--      , boundConfig = defaultBoundConfig
--      }
--      where
--        nmax = 6
--        (deltaSig, deltaEps, theta) = fromV v
--        deltaExts = toV (deltaSig, deltaEps)
--        --th = fromV theta 
--        lambda = thetaVectorApprox 1e-16 theta 
--
--
--boundsProgram "IsingSigEpsCT_dual_jump_test_nmax6" =
--  local (setJobType (MPIJob 1 6) . setJobTime (25*minute)) $ void $
--  remoteComputeBoundWithFileTreatment keepAllFiles defaultCFTBoundFiles $
--  bound (toV (0.518149, 1.412625), thetaVectorApprox 1e-16 0.96926)
--  where
--    nmax = 6
--    bound (deltaExts, lambda) = CFTBound
--      { boundKey = (isingFeasibleDefaultGaps deltaExts lambda nmax)
--        { objective  = Ising.StressTensorOPEBound (Just lambda) UpperBound
--        , spectrum   = setGaps [ ((0, Ising.Z2Even), 3)
--                               , ((0, Ising.Z2Odd),  3)
--                               ] .
--                       setTwistGap 1e-5 $
--                       unitarySpectrum
--        }
--      , precision = (blockParamsNmax nmax).precision
--      , solverParams = (optimizationParams nmax)
--        { precision = 768
--        , dualErrorThreshold = 1e-60
--        , primalErrorThreshold = 1e-60
--        }
--      , boundConfig = defaultBoundConfig
--      }
--
boundsProgram "sleep" = do
  Log.text "Sleeping for 5 seconds"
  liftIO (threadDelay (5*1000*1000))

boundsProgram p = unknownProgram p 
