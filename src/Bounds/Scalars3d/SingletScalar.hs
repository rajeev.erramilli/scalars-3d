{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE StaticPointers      #-}
{-# LANGUAGE TypeFamilies        #-}
{-# LANGUAGE TypeOperators       #-}

module Bounds.Scalars3d.SingletScalar where

import Blocks                        (Coordinate (ZZb), CrossingMat, Delta (..),
                                      DerivMultiplier (..), Derivative,
                                      TaylorCoeff (..), Taylors, crossOdd,
                                      zzbTaylors)
import Blocks                        qualified as Blocks
import Blocks.ScalarBlocks           qualified as SB
import Blocks.ScalarBlocks.Build     (scalarBlockBuildLink)
import Bootstrap.Bounds              (FourPointFunctionTerm, HasRep (..),
                                      OPECoefficient, Spectrum (..),
                                      crossingMatrix, derivsVec, listDeltas,
                                      mapBlocks, opeCoeffIdentical_, runTagged)
import Bootstrap.Bounds              qualified as Bounds
import Bootstrap.Build               (FetchConfig (..), SomeBuildChain (..),
                                      noDeps)
import Bootstrap.Math.DampedRational qualified as DR
import Bootstrap.Math.FreeVect       (FreeVect, vec)
import Bootstrap.Math.Linear.Literal (fromM, toV)
import Bootstrap.Math.VectorSpace    (zero)
import Control.Monad.IO.Class        (liftIO)
import Data.Aeson                    (FromJSON, ToJSON)
import Data.Binary                   (Binary)
import Data.Data                     (Typeable)
import Data.Functor.Compose          (Compose (..))
import Data.Reflection               (Reifies, reflect)
import Data.Tagged                   (Tagged (..), untag)
import Data.Vector                   (Vector)
import GHC.Generics                  (Generic)
import GHC.TypeNats                  (KnownNat)
import Hyperion                      (Dict (..), Static (..))
import Hyperion.Bootstrap.Bound      (BuildInJob, SDPFetchBuildConfig (..),
                                      ToSDP (..), blockDir)
import Linear.V                      (V)
import SDPB qualified

data ExternalOp s = Phi
  deriving (Show, Eq, Ord, Enum, Bounded)

instance (Reifies s Rational) => HasRep (ExternalOp s) (SB.ScalarRep 3) where
  rep x@Phi = SB.ScalarRep $ reflect x

data SingletScalar = SingletScalar
  { deltaPhi    :: Rational
  , spectrum    :: Spectrum Int
  , objective   :: Objective
  , spins       :: [Int]
  , blockParams :: SB.ScalarBlockParams
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data Objective
  = Feasibility
  | CTLowerBound
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

crossingEquations :: forall s a b .
     FourPointFunctionTerm (ExternalOp s) (SB.Standard4PtStruct, DerivMultiplier) b a
  -> V 1 (Taylors 'ZZb, FreeVect b a)
crossingEquations g0 = toV
  (zzbTaylors crossOdd, g0 Phi Phi Phi Phi (SB.Standard4PtStruct, SChannel))

singletScalarDerivsVec :: SingletScalar -> V 1 (Vector (TaylorCoeff (Derivative 'ZZb)))
singletScalarDerivsVec i = fmap ($ nmax) (derivsVec crossingEquations)
  where
    nmax = (blockParams i).nmax

singletScalarCrossingMat
  :: forall j s a. (KnownNat j, Fractional a, Eq a)
  => V j (OPECoefficient (ExternalOp s) (SB.Standard3PtStruct 3) a)
  -> Tagged s (CrossingMat j 1 (SB.ScalarBlock 3) a)
singletScalarCrossingMat channel =
  pure $ mapBlocks SB.ScalarBlock $
  crossingMatrix channel (crossingEquations @s)

internalMat
  :: forall s a . (Reifies s Rational, Fractional a, Eq a)
  => SB.SymTensorRep 3
  -> Tagged s (CrossingMat 1 1 (SB.ScalarBlock 3) a)
internalMat r = singletScalarCrossingMat $ toV $
  opeCoeffIdentical_ (Phi @s) r (vec ())

identityMat
  :: forall a s . (Fractional a, Eq a, Reifies s Rational)
  => Tagged s (CrossingMat 1 1 (SB.ScalarBlock 3) a)
identityMat = singletScalarCrossingMat (toV identityOpe)
  where
    identityRep = SB.SymTensorRep (Blocks.Fixed 0) 0
    identityOpe (o1 :: ExternalOp s) o2
      | o1 == o2  = vec (SB.Standard3PtStruct (rep o1) (rep o2) identityRep)
      | otherwise = zero

stressRep :: SB.SymTensorRep 3
stressRep = SB.SymTensorRep (RelativeUnitarity 0) 2

getInternalBlockVec
  :: (Blocks.BlockFetchContext (SB.ScalarBlock 3) a f, Applicative f, RealFloat a)
  => SB.SymTensorRep 3
  -> SingletScalar
  -> f (DR.DampedRational (Blocks.BlockBase (SB.ScalarBlock 3) a) Vector a)
getInternalBlockVec intRep i =
  fmap (DR.mapNumerator (fromM . getCompose)) $
  Bounds.getContinuumMat (blockParams i) (singletScalarDerivsVec i) $
  runTagged (deltaPhi i) $
  internalMat intRep

bulkConstraints
  :: forall a s m.
     ( RealFloat a, Binary a, Typeable a
     , Reifies s Rational
     , Blocks.BlockFetchContext (SB.ScalarBlock 3) a m
     , Applicative m
     )
  => SingletScalar
  -> Tagged s [SDPB.PositiveConstraint m a]
bulkConstraints i = pure $ do
  l <- filter even i.spins
  (delta, range) <- listDeltas l i.spectrum
  let r = SB.SymTensorRep delta l
  pure $ Bounds.bootstrapConstraint i.blockParams dv range $ untag @s (internalMat r)
  where
    dv = singletScalarDerivsVec i

singletScalarSDP
  :: forall a m .
     ( RealFloat a, Applicative m, Binary a, Typeable a
     , Blocks.BlockFetchContext (SB.ScalarBlock 3) a m
     )
  => SingletScalar
  -> SDPB.SDP m a
singletScalarSDP i = runTagged i.deltaPhi $ do
  bulk   <- bulkConstraints i
  unit   <- identityMat
  stress <- internalMat stressRep
  let dv = singletScalarDerivsVec i
      (cons, obj, norm) = case i.objective of
        Feasibility ->
          ( bulk
          , Bounds.bootstrapObjective     i.blockParams dv $ zero `asTypeOf` unit
          , Bounds.bootstrapNormalization i.blockParams dv unit
          )
        CTLowerBound ->
          ( bulk
          , Bounds.bootstrapObjective     i.blockParams dv unit
          , Bounds.bootstrapNormalization i.blockParams dv stress
          )
  pure $ SDPB.SDP
    { SDPB.objective           = obj
    , SDPB.normalization       = norm
    , SDPB.positiveConstraints = cons
    }

instance ToSDP SingletScalar where
  type SDPFetchKeys SingletScalar = '[ SB.BlockTableKey ]
  toSDP = singletScalarSDP

instance SDPFetchBuildConfig SingletScalar where
  sdpFetchConfig _ _ boundFiles =
    liftIO . SB.readBlockTable (blockDir boundFiles) :&: FetchNil
  sdpDepBuildChain _ bConfig boundFiles =
    SomeBuildChain $ noDeps $ scalarBlockBuildLink bConfig boundFiles False

instance Static (Binary SingletScalar)              where closureDict = static Dict
instance Static (Show SingletScalar)                where closureDict = static Dict
instance Static (ToSDP SingletScalar)               where closureDict = static Dict
instance Static (ToJSON SingletScalar)              where closureDict = static Dict
instance Static (SDPFetchBuildConfig SingletScalar) where closureDict = static Dict
instance Static (BuildInJob SingletScalar)          where closureDict = static Dict
